package com.trainee.endproject

import android.app.Application
import android.content.res.Configuration
import com.trainee.data.apiModule
import com.trainee.data.retrofitModule
import com.trainee.endproject.di.applicationModule
import com.trainee.endproject.di.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TvShowApplication: Application() {


    override fun onCreate() {
        super.onCreate()
        instance = this

        startKoin {
            androidContext(this@TvShowApplication)
            modules(appModules + dataModules)
        }
    }
    companion object {
        lateinit var instance: Application
            private set
    }

}
val appModules = listOf(applicationModule, uiModule)

val dataModules = listOf(apiModule, retrofitModule)

