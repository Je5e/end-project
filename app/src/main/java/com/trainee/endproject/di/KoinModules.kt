package com.trainee.endproject.di

import android.content.Context
import android.content.SharedPreferences
import com.trainee.endproject.R
import com.trainee.endproject.ui.auth.AuthenticationViewModel
import com.trainee.endproject.ui.tvshow.HomeViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module {
    single<SharedPreferences> {
        androidContext().getSharedPreferences(
            androidContext().getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
    }

}

val uiModule = module {
    viewModel { AuthenticationViewModel(get(), get(),get()) }
    viewModel { HomeViewModel(get()) }
}