package com.trainee.endproject.ui.auth

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import androidx.transition.Fade
import androidx.transition.Slide
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import com.google.android.material.snackbar.Snackbar
import com.trainee.data.data.remote.service.auth.CreateSessionRequest
import com.trainee.domain.model.Resource
import com.trainee.endproject.R
import com.trainee.endproject.utils.gone
import com.trainee.endproject.utils.safe
import com.trainee.endproject.utils.visible
import kotlinx.android.synthetic.main.fragment_logged_out.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoggedOutFragment : Fragment() {

    private val authenticationViewModel: AuthenticationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO: si el usuario tiene session, navegar al perfil
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_logged_out, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authWebView.apply {
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    if (url?.contains("allow") == true) {
                        handleAuthorizationSuccessful(
                            authenticationViewModel.requestToken.value!!
                        )
                    }
                }
            }
            settings.javaScriptEnabled = true
        }

        btLogin.setOnClickListener {

            authenticationViewModel.getRequestToken()

            val transition = TransitionSet()
            transition.apply {
                ordering = TransitionSet.ORDERING_SEQUENTIAL
                addTransition(
                    Slide(Gravity.TOP)
                        .addTarget(ivKey)
                        .addTarget(tvNeedToLogin)
                        .addTarget(btLogin)
                        .setDuration(300)
                )
                addTransition(
                    Slide(Gravity.BOTTOM)
                        .addTarget(authWebView)
                        .setDuration(300)
                )
                interpolator = FastOutSlowInInterpolator()
            }
            TransitionManager.beginDelayedTransition(container, transition)
            infoGroup.gone()
            webGroup.visible()

            authenticationViewModel.requestToken.observe(viewLifecycleOwner, { tokenResource ->
                when (tokenResource) {
                    is Resource.Success -> {
                        authorizeToken(tokenResource.data)
                    }
                    is Resource.Error -> {
                        {} // mainViewModel.showSnackbar(R.string.error_login)
                    }
                    is Resource.Loading -> {
                        // TODO handle this
                    }
                    else -> {}
                }.safe
            })
        }
    }

    private fun authorizeToken(token: String) {
        authWebView.loadUrl("https://www.themoviedb.org/authenticate/$token")
    }

    private fun handleAuthorizationSuccessful(token: Resource<String>) {
        when (token) {
            is Resource.Success -> {
                authenticationViewModel.createSession(CreateSessionRequest(token.data))

                val transition = TransitionSet()
                transition.apply {
                    ordering = TransitionSet.ORDERING_SEQUENTIAL
                    addTransition(
                        Fade()
                            .addTarget(authWebView)
                            .setDuration(200)
                    )
                    addTransition(
                        Fade()
                            .addTarget(pbLoading)
                            .addTarget(tvPleaseWait)
                            .setDuration(200)
                    )
                }

                TransitionManager.beginDelayedTransition(container, transition)
                webGroup.gone()
                loadingGroup.visible()

            }
            else -> {
                // TODO Handle this
            }
        }.safe

        authenticationViewModel.accountDetails.observe(viewLifecycleOwner, Observer { accountDetailsResource ->
            when (accountDetailsResource) {
                is Resource.Success -> {
                   Toast.makeText (context,"Login success",Toast.LENGTH_LONG).show()
                    Log.d("LOGIN","SUCCESS!!!!")
                    findNavController().popBackStack()
                }
                is Resource.Error -> {
                    view?.let { Snackbar.make(it, accountDetailsResource.errorMessage, Snackbar.LENGTH_SHORT).show() }
                }
                is Resource.Loading -> {
                    // TODO Handle this
                    Unit
                }
            }.safe
        })
    }
}