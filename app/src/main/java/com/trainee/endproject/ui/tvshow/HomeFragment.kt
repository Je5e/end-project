package com.trainee.endproject.ui.tvshow

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.trainee.endproject.R
import com.trainee.endproject.databinding.FragmentHomeBinding
import com.trainee.endproject.ui.auth.AuthenticationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModel()

    private lateinit var binding: FragmentHomeBinding

    private lateinit var listAdapter: TvShowGridAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentHomeBinding.inflate(inflater)

        binding.viewModel = homeViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = this.viewLifecycleOwner
        setupListAdapter()
        //setupNavigation()
    }

    private fun setupListAdapter() {
        val viewModel = binding.viewModel
        if (viewModel != null) {
            listAdapter = TvShowGridAdapter(viewModel)
            binding.recyclerView.adapter = listAdapter
        } else {
            Log.d("ViewModel not initialized when attempting to set up adapter.","Error")
        }
    }




}