package com.trainee.endproject.ui.tvshow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.trainee.data.data.remote.ApiManager
import com.trainee.data.tvshows.TvShow
import com.trainee.domain.model.Resource
import com.trainee.endproject.utils.SingleLiveEvent
import com.trainee.endproject.utils.disposeWith
import com.trainee.endproject.utils.log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeoutException

class HomeViewModel(private val apiManager: ApiManager) : ViewModel() {

    private val _tvShows = MutableLiveData<List<TvShow>>()
    private val _message = SingleLiveEvent<String>()
    private val compositeDisposable = CompositeDisposable()
    val tvShows: LiveData<List<TvShow>>
        get() = _tvShows

    val message: LiveData<String>
        get() = _message

    fun getTvShowsPopular() {
        apiManager
            .getTvShowsPopular()
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { tvShowsPopular ->
                    if (tvShowsPopular is Resource.Success) {
                        _tvShows.postValue(tvShowsPopular.data.tvshows)
                    }
                },
                onError = { error -> handleError(error, "get-request-token") }
            )
            .disposeWith(compositeDisposable)
    }

    private fun handleError(error: Throwable, caller: String) {
        error.localizedMessage?.let {
            log("ERROR $caller -> $it")
        } ?: log("ERROR $caller ->")
            .also {
                error.printStackTrace()
            }
        when (error) {
            is IOException -> _message.postValue("Please check your internet connection")
            is TimeoutException -> _message.postValue("Request timed out")
            else -> _message.postValue("An error occurred")
        }
    }

    init {
        getTvShowsPopular()
    }
}