package com.trainee.endproject.ui.tvshow

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.leanback.widget.DiffCallback
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.trainee.data.tvshows.TvShow
import com.trainee.endproject.databinding.LayoutTvshowListItemBinding

class TvShowGridAdapter(private val viewModel: HomeViewModel) :
    ListAdapter<TvShow, TvShowGridAdapter.TvShowViewHolder>(DiffCallback) {


    class TvShowViewHolder private constructor(
        private var binding:LayoutTvshowListItemBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel:HomeViewModel , tvshow: TvShow) {
            //binding.viewModel = viewModel
            binding.tvshow = tvshow
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): TvShowViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = LayoutTvshowListItemBinding.inflate(layoutInflater, parent, false)

                return TvShowViewHolder(binding)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        return TvShowViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) {
        val tvshow = getItem(position)

        holder.bind(viewModel, tvshow)
    }


    companion object DiffCallback : DiffUtil.ItemCallback<TvShow>() {
        override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem.posterPath == newItem.posterPath
        }
    }
}