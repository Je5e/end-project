package com.trainee.domain

object Constants {

    const val KEY_SESSION_ID = "session-id"

    const val KEY_MOVIE_ID = "movie-id"

    const val KEY_TRANSITION_NAME = "transition-name"

    const val KEY_IS_AUTHENTICATED: String = "isAuthenticated"

    const val KEY_ACCOUNT_ID: String = "accountId"

    const val KEY_ACTOR_ID: String = "actorId"

    const val KEY_COUNTRY_CODE: String = "country"

    const val KEY_COUNTRY_NAME: String = "countryName"
}