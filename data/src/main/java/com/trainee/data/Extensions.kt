package com.trainee.data

import com.trainee.data.data.remote.service.common.GeneralTvShowResponse
import com.trainee.data.data.remote.utils.Config
import com.trainee.data.tvshows.TvShow

internal fun GeneralTvShowResponse.toTvShow(): TvShow {
    return TvShow(
        this.originalName,
        this.overview,
        this.name,
        this.id,
        this.backdropPath.getBackdropUrl(),
        this.popularity,
        this.posterPath.getPosterUrl(),
        this.voteAverage,
        this.firstAirDate,
        this.originalLanguage,
        null,
        null,
        this.voteCount
    )
}


internal fun String?.getPosterUrl(): String {
    return this?.let {
        "${Config.BASE_IMAGE_URL}${Config.DEFAULT_POSTER_SIZE}$it"
    } ?: ""
}

internal fun String?.getBackdropUrl(): String {
    return this?.let {
        "${Config.BASE_IMAGE_URL}${Config.DEFAULT_BACKDROP_SIZE}${this}"
    } ?: ""
}

internal fun String?.getProfilePictureUrl(): String {
    return this?.let {
        "${Config.BASE_IMAGE_URL}${Config.SMALL_PROFILE_PICTURE_SIZE}$this"
    } ?: ""
}