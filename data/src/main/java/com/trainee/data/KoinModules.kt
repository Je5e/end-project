package com.trainee.data

import android.content.SharedPreferences
import com.haroldadmin.cnradapter.CoroutinesNetworkResponseAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.trainee.data.data.remote.ApiManager
import com.trainee.data.data.remote.service.account.AccountService
import com.trainee.data.data.remote.service.auth.AuthenticationService
import com.trainee.data.data.remote.service.discover.DiscoveryService
import com.trainee.data.data.remote.utils.ApiKeyInterceptor
import com.trainee.data.data.remote.utils.Config
import com.trainee.data.data.remote.utils.SessionIdInterceptor
import com.trainee.domain.Constants
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*


/*val retrofitModule = module {
    single { Cache(androidContext().cacheDir, 50 * 1024 * 1024) }

    single { (sessionId: String) -> SessionIdInterceptor(sessionId) }

    single { (apiKey: String) -> ApiKeyInterceptor(apiKey) }

    single { (loggingLevel: HttpLoggingInterceptor.Level) ->
        HttpLoggingInterceptor().apply {
            level = loggingLevel
        }
    }

    single {

        val apiKey = BuildConfig.API_KEY
        val sessionId = get<SharedPreferences>().getString(Constants.KEY_SESSION_ID, "")

        OkHttpClient.Builder()
            .addInterceptor(get<ApiKeyInterceptor> {
                parametersOf(apiKey)
            })
            .addInterceptor(get<SessionIdInterceptor> {
                parametersOf(sessionId)
            })
            .also {
                if (BuildConfig.DEBUG)
                    it.addInterceptor(get<HttpLoggingInterceptor> {
                        parametersOf(HttpLoggingInterceptor.Level.BASIC)
                    })
            }
            .cache(get())
            .build()
    }

   *//* single {
        val adapter = SafeRfc3339DateJsonAdapter(Rfc3339DateJsonAdapter()).nullSafe()
        Moshi.Builder()
            .add(Date::class.java, adapter)
            .addLast(KotlinJsonAdapterFactory())
            .build()
            .let { moshi -> MoshiConverterFactory.create(moshi) }
    }*//*
    single {
        val adapter = SafeRfc3339DateJsonAdapter(Rfc3339DateJsonAdapter()).nullSafe()
        Moshi.Builder()
            .addLast(com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory())
            .build()
            .let { moshi -> MoshiConverterFactory.create(moshi) }
    }

    single { CoroutinesNetworkResponseAdapterFactory() }

    single {
        Retrofit.Builder()
            .client(get())
            .addCallAdapterFactory(get<CoroutinesNetworkResponseAdapterFactory>())
            .addConverterFactory(get<MoshiConverterFactory>())
            .baseUrl(Config.BASE_URL)
            .build()
    }

}*/

val retrofitModule = module {
    single { Cache(androidContext().cacheDir, 50 * 1024 * 1024) }

    single { (sessionId: String) -> SessionIdInterceptor(sessionId) }

    single { (apiKey: String) -> ApiKeyInterceptor(apiKey) }

    single { (loggingLevel: HttpLoggingInterceptor.Level) ->
        HttpLoggingInterceptor().apply {
            level = loggingLevel
        }
    }

    single {

        val apiKey = BuildConfig.API_KEY
        val sessionId = get<SharedPreferences>().getString(Constants.KEY_SESSION_ID, "")

        OkHttpClient.Builder()
            .addInterceptor(get<ApiKeyInterceptor> {
                parametersOf(apiKey)
            })
            .addInterceptor(get<SessionIdInterceptor> {
                parametersOf(sessionId)
            })
            .also {
                if (BuildConfig.DEBUG)
                    it.addInterceptor(get<HttpLoggingInterceptor> {
                        parametersOf(HttpLoggingInterceptor.Level.BASIC)
                    })
            }
            .cache(get())
            .build()
    }

    single {
        val adapter = SafeRfc3339DateJsonAdapter(Rfc3339DateJsonAdapter()).nullSafe()
        Moshi.Builder()
            //.add(com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory())
            .add(Date::class.java, adapter)
            .build()
            .let { moshi -> MoshiConverterFactory.create(moshi) }
    }


    single { CoroutinesNetworkResponseAdapterFactory() }

    single {
        Retrofit.Builder()
            .client(get())
            .addCallAdapterFactory(get<CoroutinesNetworkResponseAdapterFactory>())
            .addConverterFactory(get<MoshiConverterFactory>())
            .baseUrl(Config.BASE_URL)
            .build()
    }
}

val apiModule = module {
    single { get<Retrofit>().create(AuthenticationService::class.java) }
    single { get<Retrofit>().create(DiscoveryService::class.java) }
    single { get<Retrofit>().create(AccountService::class.java) }
    single { ApiManager(get(), get(),get()) }

    /* single { get<Retrofit>().create(DiscoveryService::class.java) }
     single { get<Retrofit>().create(SearchService::class.java) }
     single { get<Retrofit>().create(MovieService::class.java) }
     single { get<Retrofit>().create(AccountService::class.java) }
     single { get<Retrofit>().create(PersonService::class.java) }
     single { ApiManager(get(), get()) }*/
}