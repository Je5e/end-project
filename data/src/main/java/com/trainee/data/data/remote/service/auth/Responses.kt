package com.trainee.data.data.remote.service.auth

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize

data class RequestTokenResponse(
    @field:Json(name="success") val success: Boolean,
    @field:Json(name="expires_at") val expiresAt: String,
    @field:Json(name="request_token") val requestToken: String): Parcelable


@Parcelize
data class CreateSessionResponse(
    @field:Json(name="success") val success: Boolean,
    @field:Json(name="session_id") val sessionId: String): Parcelable