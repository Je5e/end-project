package com.trainee.data.data.remote.service.common

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ErrorResponse(
    @field:Json(name = "status_code") val statusCode: String,
    @field:Json(name = "status_message") val statusMessage: String
) : Parcelable

@Parcelize
internal data class GeneralTvShowResponse (
    @field:Json(name="original_name") val originalName: String,
    @field:Json(name="overview") val overview: String?,
    @field:Json(name="name") val name: String,
    @field:Json(name="id") val id: Int,
    @field:Json(name="backdrop_path") var backdropPath: String?,
    @field:Json(name="popularity") val popularity: Double,
    @field:Json(name="poster_path") var posterPath: String?,
    @field:Json(name="vote_average") var voteAverage: Double,
    @field:Json(name="first_air_date") val firstAirDate: String,
    @field:Json(name="original_language") val originalLanguage: String,
    @field:Json(name="genre_ids") val genreIds: List<Int>,
    @field:Json(name="origin_country") val originCountry: List<String>,
    @field:Json(name="vote_count") val voteCount: Int): Parcelable