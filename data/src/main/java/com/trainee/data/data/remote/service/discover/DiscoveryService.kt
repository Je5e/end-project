package com.trainee.data.data.remote.service.discover

import com.haroldadmin.cnradapter.NetworkResponse
import com.trainee.data.data.remote.service.common.ErrorResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

internal interface DiscoveryService {


    @GET("tv/popular")
    fun getPopularTvShows(): Deferred<NetworkResponse<DiscoverTvShowsResponse, ErrorResponse>>

    @GET("tv/top_rated")
    fun getTopRatedTvShows(): Deferred<NetworkResponse<DiscoverTvShowsResponse, ErrorResponse>>

    @GET("tv/on_the_air")
    fun getOnTheAirTvShows(): Deferred<NetworkResponse<DiscoverTvShowsResponse, ErrorResponse>>

    @GET("tv/airing_today")
    fun getAiringTodayTvShows(): Deferred<NetworkResponse<DiscoverTvShowsResponse, ErrorResponse>>

}