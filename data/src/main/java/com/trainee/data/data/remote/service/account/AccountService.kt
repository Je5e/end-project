package com.trainee.data.data.remote.service.account

import com.haroldadmin.cnradapter.NetworkResponse
import com.trainee.data.data.remote.service.common.ErrorResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface AccountService {

    @GET("account")
    fun getAccountDetails(): Deferred<NetworkResponse<AccountDetailsResponse, ErrorResponse>>
}