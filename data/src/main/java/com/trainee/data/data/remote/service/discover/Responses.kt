package com.trainee.data.data.remote.service.discover

import android.os.Parcelable
import com.squareup.moshi.Json
import com.trainee.data.data.remote.service.common.GeneralTvShowResponse
import com.trainee.data.data.remote.service.tv.TvShowResponse
import com.trainee.data.tvshows.TvShow
import kotlinx.android.parcel.Parcelize


@Parcelize
internal data class DiscoverTvShowsResponse(
    @field:Json(name="page") val page: Int,
    @field:Json(name="results") val results: List<GeneralTvShowResponse>,
    @field:Json(name="total_results") val totalResults: Int,
    @field:Json(name="total_pages") val totalPages: Int): Parcelable

data class Collection(

    val name: String,

    val contents: List<Int>
) {

    var tvshows: List<TvShow>? = null
}