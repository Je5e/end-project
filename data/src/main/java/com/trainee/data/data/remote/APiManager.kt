package com.trainee.data.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.trainee.data.data.remote.service.account.AccountDetailsResponse
import com.trainee.data.data.remote.service.account.AccountService
import com.trainee.data.data.remote.service.auth.AuthenticationService
import com.trainee.data.data.remote.service.auth.CreateSessionRequest
import com.trainee.data.data.remote.service.discover.Collection
import com.trainee.data.data.remote.service.discover.DiscoveryService
import com.trainee.data.toTvShow
import com.trainee.data.tvshows.TvShow
import com.trainee.domain.model.Resource
import io.reactivex.Single
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.rx2.asSingle

class ApiManager internal constructor(private val authenticationService: AuthenticationService,
private val discoveryService: DiscoveryService,
private val accountService: AccountService) {



    fun getRequestToken(): Single<Resource<String>> {
        return authenticationService
            .getRequestToken()
            .asSingle(Dispatchers.Default)
            .flatMap { response ->
                Single.just(
                    when (response) {
                        is NetworkResponse.Success -> {
                            Resource.Success(response.body.requestToken)
                        }
                        is NetworkResponse.ServerError -> {
                            Resource.Error<String>(response.body?.statusMessage ?: "Server Error")
                        }
                        is NetworkResponse.NetworkError -> {
                            Resource.Error(response.error.localizedMessage ?: "Network Error")
                        }

                    }
                )
            }
    }

    fun createSession(request: CreateSessionRequest): Single<Resource<String>> {
        return authenticationService.createNewSession(request)
            .asSingle(Dispatchers.Default)
            .flatMap { response ->
                Single.just(
                    when (response) {
                        is NetworkResponse.Success -> {
                            Resource.Success(response.body.sessionId)
                        }
                        is NetworkResponse.ServerError -> {
                            Resource.Error<String>(response.body?.statusMessage ?: "Server Error")
                        }
                        is NetworkResponse.NetworkError -> {
                            Resource.Error(response.error.localizedMessage ?: "Network Error")
                        }
                    }
                )
            }
    }

    fun getTvShowsPopular(): Single<Resource<Collection>> {
        return discoveryService
            .getPopularTvShows()
            .asSingle(Dispatchers.Default)
            .flatMap { response ->
                Single.just(
                    when (response) {
                        is NetworkResponse.Success -> {
                            Resource.Success(
                                Collection(
                                    "Favorite",
                                    response.body.results.map { it.id }
                                ).apply {
                                    this.tvshows = response.body.results.map { it.toTvShow() }
                                }
                            )
                        }
                        is NetworkResponse.ServerError -> {
                            Resource.Error<com.trainee.data.data.remote.service.discover.Collection>(response.body?.statusMessage ?: "Server Error")
                        }
                        is NetworkResponse.NetworkError -> {
                            Resource.Error(response.error.localizedMessage ?: "Network Error")
                        }

                    }
                )
            }
    }

    fun getAccountDetails(): Single<Resource<AccountDetailsResponse>> {
        return accountService
            .getAccountDetails()
            .asSingle(Dispatchers.Default)
            .flatMap { response ->
                Single.just(
                    when (response) {
                        is NetworkResponse.Success -> {
                            Resource.Success(response.body)
                        }
                        is NetworkResponse.ServerError -> {
                            Resource.Error<AccountDetailsResponse>(response.body?.statusMessage ?: "Server Error")
                        }
                        is NetworkResponse.NetworkError -> {
                            Resource.Error(response.error.localizedMessage ?: "Network Error")
                        }
                    }
                )
            }
    }
}