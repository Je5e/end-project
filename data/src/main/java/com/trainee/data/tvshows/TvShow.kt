package com.trainee.data.tvshows

// Entity
 data class TvShow (
    val originalName: String,
    val overview: String?,
    val name: String,
    val id: Int,
    var backdropPath: String?,
    val popularity: Double,
    var posterPath: String?,
    var voteAverage: Double?,
    val firstAirDate: String,
    val originalLanguage: String,
    val genreIds: List<Int>?,
    val originCountry: List<String>?,
    val voteCount: Int)